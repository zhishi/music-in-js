合成器 = new WebAudioTinySynth();
// TODO：将 乐器->序号 常量化
选音色(107)

function 选音色(序号) {
  合成器.send([0xc0, 序号]);
}

function 播放音符(序号) {
  if (序号 == null)
    return
  合成器.send([0x90, 默认音符 + 序号, 100]);
}

function 播放(曲谱) {
  时间点 = 0
  for (索引 in 曲谱) {
    音 = 曲谱[索引]
    音阶 = 音.length == 2 ? 默认音阶 : 音[2]
    setTimeout(function(序号) {
      return function() { 播放音符(序号) }
    }(取音符序号(音阶, 音[0])), 时间点)
    时间点 += 音[1] * 短
  }
}

function 取音符序号(音阶, 音符) {
  if (音符 == null)
    return null
  return (音阶 - 默认音阶) * 12 + 音符
}
