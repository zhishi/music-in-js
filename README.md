用 [Web Audio API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API) 和纯 JavaScript 生成简单旋律，不需音频资源文件。

在线演示 [在此](http://zhishi.gitee.io/music-in-js/)。如需本地运行，可下载源码，用火狐或 Chrome 浏览器（已测过 Mac 和 Android，但 iOS 尚未成功）打开 html 文件后点击“开始”即可听到旋律。

可选多种乐器音色如古筝、三弦、鼓、吉他等。现有曲谱：
- 我是一个兵
- 娘子军连歌
- 劳动最光荣
- 彩云追月
- 小鸭子
- 两只老虎
- 粉刷匠

### 待做

- 添加音色
  - 加入号等乐器，需要加入衰减
  - 按打击、丝弦等分类
  - 近似人哼的音色
- 通过 [createPeriodicWave](https://developer.mozilla.org/en-US/docs/Web/API/BaseAudioContext/createPeriodicWave) 为需要的音色定制波形

### 参考

- [Generate Sounds Programmatically With Javascript](http://marcgg.com/blog/2016/11/01/javascript-audio/)
- [Examples of acoustic instruments producing sounds very close to pure square, triangle or sawtooth waves](https://music.stackexchange.com/questions/16980/examples-of-acoustic-instruments-producing-sounds-very-close-to-pure-square-tri)
